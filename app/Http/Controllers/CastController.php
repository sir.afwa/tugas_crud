<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\http\Request;

class CastController extends Controller
{
    public function index()
    {$cast = DB::table('cast')->get();
        return view('cast.index', compact('cast'));

    }
    public function create()
    {
        return view('cast.create');
    }
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|unique:cast',
            'umur' => 'required',
            'bio' => 'required',
        ]);
        $query = DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio'],
        ]);
        return redirect('/cast/create');
    }

    public function show($id)
    {
        $post = DB::table('cast')->where('id', $id)->first();
        return view('cast.show', compact('cast'));
    }
    public function edit($id)
    {
        $post = DB::table('cast')->where('id', $id)->first();
        return view('cast.edit', compact('cast'));

    }
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|unique:cast',
            'umur' => 'required|max:2',
            'bio' => 'required|max:255',
        ]);
        $query = DB::table('cast')
            ->where('id', $id)
            ->update([
                'name' => $request['nama'],
                'body' => $request['body'],
                'bio' => $request['bio'],
            ]);
        return redirect('/cast');
    }
    public function destroy($id)
    {
        $query = DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast');

    }
}
