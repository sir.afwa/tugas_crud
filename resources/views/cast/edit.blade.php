@extends('layout.master')

$@section('header')
Halaman Tambah Pemain
    
@endsection
  
@section('title')
    Halaman Tambah Pemain
    @endsection

@section('isi')


<div>
      <form action="/cast/{{$cast->id}}" method="POST">
        @method('put')
            @csrf
            <div class="form-group">
                <label>Nama Pemain</label>
                <input type="text" class="form-control" name="nama" value="{{$cast->nama}}" placeholder="Masukkan nama Pemain">
                <label>Umur Pemain</label>
                <input type="text" class="form-control" name="umur" value="{{$cast->umur}}" placeholder="Masukkan umur Pemain">
                <label>Biodata Pemain</label>
                <input type="text" class="form-control" name="bio" value="{{$cast->bio}}" placeholder="Masukkan biodata Pemain">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror

                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror

                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>
@endsection